package co.vulpin.searx.client.entities

import com.google.gson.annotations.SerializedName

class SearchResult {

    String engine
    List<String> engines
    String title
    String url
    @SerializedName("parsed_positions") List<Integer> parsedPositions
    @SerializedName("parsed_url") List<String> parsedUrl
    String content
    @SerializedName("pretty_url") String prettyUrl
    double score

}
