package co.vulpin.searx.client

import co.vulpin.searx.client.entities.SearchResults
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Builder
import okhttp3.Request

class SearchBuilder {

    private SearxApiClient client

    private Builder urlBuilder

    protected SearchBuilder(SearxApiClient client, HttpUrl baseUrl) {
        this.client = client
        this.urlBuilder = baseUrl.newBuilder()
    }

    SearchBuilder q(String q) {
        urlBuilder.addQueryParameter("q", q)
        return this
    }

    SearchBuilder categories(String... categories) {
        urlBuilder.addQueryParameter("categories", categories.join(","))
        return this
    }

    SearchBuilder engines(String... engines) {
        urlBuilder.addQueryParameter("engines", engines.join(","))
        return this
    }

    SearchBuilder lang(String lang) {
        urlBuilder.addQueryParameter("lang", lang)
        return this
    }

    SearchBuilder pageNo(int pageNo) {
        urlBuilder.addQueryParameter("pageno", pageNo as String)
        return this
    }

    SearchBuilder safeSearch(boolean safeSearch) {
        urlBuilder.addQueryParameter("safesearch", safeSearch as String)
        return this
    }

    SearchResults execute() {
        def url = urlBuilder
            .addQueryParameter("format", "json")
            .build()

        def req = new Request.Builder()
            .url(url)
            .get()
            .build()

        def call = client.httpClient.newCall(req)

        def resp = call.execute()

        def body = resp.body().string()

        return client.gson.fromJson(body, SearchResults)
    }

}
