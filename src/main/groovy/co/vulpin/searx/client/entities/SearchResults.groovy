package co.vulpin.searx.client.entities

import com.google.gson.annotations.SerializedName

class SearchResults {

    @SerializedName("number_of_results") int numberOfResults
    List<SearchResult> results
    String query
    // I'm not sure what type this is as I haven't seen any responses where
    // this value isn't an empty list
    //List answers
    List<String> suggestions
    @SerializedName("infoboxes") List infoBoxes

}
