package co.vulpin.searx.client

import com.google.gson.Gson
import okhttp3.HttpUrl
import okhttp3.OkHttpClient

class SearxApiClient {

    protected final Gson gson
    protected final OkHttpClient httpClient

    SearxApiClient() {
        this.httpClient = new OkHttpClient()
        this.gson = new Gson()
    }

    SearchBuilder search(String baseUrl) {
        return new SearchBuilder(this, HttpUrl.parse(baseUrl))
    }

}
