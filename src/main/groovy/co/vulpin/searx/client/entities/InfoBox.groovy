package co.vulpin.searx.client.entities

class InfoBox {

    String engine
    String content
    String infobox
    String imgSrc
    String id
    List<Url> urls

}
